<!-- This form is for Fedora contributors to receive swag for organizing an event. This form should only be used once you have an approved, public Fedora Mindshare Committee ticket.

NOTE: Make sure your issue is marked as confidential. This way, only members of the Fedora Mindshare Committee will receive your request. This box should be checked when you submit the issue: "This issue is confidential and should only be visible to team members with at least Reporter access."

If you have questions about this form, send them by email to fca@fedoraproject.org or add a comment to this issue once opened.-->

## About me

<!-- These questions help us know who you are and where we are sending a swag package. All questions are required unless designated as "optional". -->

* **Full legal name**<!-- Required for delivery. -->:

* **FAS username**:

* **Email**<!-- Shared with parcel delivery company, e.g. FedEx -->:

* **Phone number** with international dial code<!-- Shared with parcel delivery company, e.g. FedEx or local post office-->:

* **Company (if delivering to an office address)**:

* **Address 1**:

* **Address 2** (optional):

* **Town/City**:

* **County/State/Province (2-letter code)**:

* **Postal/Zip/Pin Code**:

* **Country**:

### Tax information

<!-- For addresses in the following countries, the following information is legally required for Fedora to send mail in your country:

* Brazil: CPF/Tax ID
* Chile, Peru: RUT/Tax ID
* Italy: Personal ID/CIE No.
* Norway, Taiwan: Personal/Tax ID
* Spain: Personal ID/DNI No.
* Turkey: VAT No/TR ID

If your address is in one of these countries, you MUST provide this identifier below or we cannot fulfill your shipment.-->

* **Tax identifier** (if applicable): 


## About my event

<!-- These questions help us identify your event and logistics for processing your reimbursement. -->

* **Name of event**:

* **Event dates**:

* **Link to public Mindshare Committee ticket**<!-- If you do not know what this means, you skipped a step. -->:


## About my swag request

<!-- The Fedora warehouse carries the following items. This template is not always up to date. Some items may be out of inventory and other new ones might have been added. If you are not sure, ask.

* Fedora 12oz Sherpa Tumbler & Can Insulator
* Teardrop Pin (classic logo)
* Neuro Fedora Sticker
* Skull Sticker
* Students Working Lenovo Sticker Sheet (classic logo)
* Rainbow Stickers (classic logo)
* Powerbank (classic logo) (Note, this is difficult to ship outside of US and EU)
* Fedora Black Lanyard (classic logo)
* Fedora Navy Trim Pen (classic logo)
-->

* **Requested swag** (with quantities):
    * **Item 1**: NAME (x AMOUNT)
    * **Item 2**: NAME (x AMOUNT)
    * **Item 3**: NAME (x AMOUNT)
    * **Item 4**: NAME (x AMOUNT)
    * **Item 5**: NAME (x AMOUNT)

* **I am feeling lucky**<!-- Change this to yes and delete the "Requested swag" question above if you prefer to take the shipper's choice of swag distribution. Answering "yes" means the shipper will decide on items and quantities. -->: yes/no


<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"check1::needs triage" ~"check2::needs triage" ~"check3::needs triage"
/confidential
