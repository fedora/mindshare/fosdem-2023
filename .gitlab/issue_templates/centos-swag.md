<!--
Use this template to request swag. If the swag is to run a booth any
other costs, use the sponsor template instead. This is for swag only.

Alternatively, consider one of the following templates:

* event: host a CentOS event, like a Classroom
* sponsor: sponsorship or booth for an existing event
* swag: swag to be printed or shipped
* travel: one person to travel to an existing event
* travel-connect: one person to travel to CentOS Connect (simpler)
-->

* **What is the swag for?**:

* **What swag do you want?**:

* **How much swag do you need?**:

* **Cost estimate**:

* **Where should we send it?**: (general region, we'll follow up privately for shipping address)

* **When do you need it by?**:
