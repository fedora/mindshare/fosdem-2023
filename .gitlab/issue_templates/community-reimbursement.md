<!-- This form is for upstream contributors of Red Hat-associated open source projects to receive reimbursement and provide documentation for their expenses at an event. This form should ONLY be used when you have received prior approval in a public ticket with the Fedora Mindshare Committee or CentOS Promo SIG.

NOTE: Make sure your issue is marked as confidential. This way, only members of the Red Hat Open Source Program Office will receive your request. This box should be checked before you open the issue: "This issue is confidential and should only be visible to team members with at least Reporter access."

If you have questions about this form, send them by email to ospo@redhat.com or add a comment to this issue after opening it. -->


## About me
<!-- These questions help us know who you are. -->

* **Full legal name**:
<!-- Required for financial transactions. -->

* **FAS username**:


## About my event
<!-- These questions help us identify your event and logistics for processing your reimbursement. -->

* **Event name**:

* **Event date(s)**: From YYYY-MM-DD to YYYY-MM-DD
<!-- Use YYYY-MM-DD date formatting. -->

* **URL to public approval ticket**:
<!-- If you do not have this, you skipped a step. Contact the Fedora or CentOS Community Architects for help completing this step. -->

* **URL to your event report and/or social media**:
<!-- All events funded by Red Hat OSPO require you to write an event report or social media posts about the event. If the event has not yet happened, write "TBD". Flock is exempted from this policy and you may write "Flock exemption" here instead (although it is still encouraged for Flock). -->

* **URL to your BEAR Compliance Request ticket** (if applicable):
<!-- https://gitlab.com/redhat/ospo/bear -->


## About my expenses
<!-- These questions help us understand your expenses and identifying the reimbursement total amount. -->

* **Total amount budgeted**:
<!-- Total amount approved in the public ticket. You must have this data before opening this issue. -->

* **Total amount spent**:
<!-- Total amount spent that you are requesting to be reimbursed. If the event has not yet happened, write "TBD". -->

* **Do you have receipts for all expenses?**: yes/no/TBD
<!-- Upload all receipts to this GitLab issue or wait for a Community Architect to create a secure Google Drive folder for you to upload them. Do this in a follow-up comment after opening the issue. If the event has not yet happened, write "TBD".

IMPORTANT: To complete the reimbursement, you must provide itemized, dated receipts. This means every receipt MUST have a clearly-printed date and any itemized expenses must be listed. For example, this means for a meal reimbursement, each item ordered must be individually listed. We will not be able to process your reimbursement without dated, itemized receipts. -->


## About my bank
<!-- Red Hat issues reimbursements as direct bank deposits. We need certain information about your bank so that Red Hat can deposit the reimbursement into your bank account. -->

* **Country**:
<!-- In what country is this bank account registered? -->

* **Currency**:
<!-- What currency do you request the reimbursement to be delivered? Use three-letter currency codes, e.g. USD, EUR, INR, GBP, etc. -->

<!-- IMPORTANT: To complete the reimbursement, you must provide a bank proof. This is an official document or letter from your bank that provides key information like your name, your account number, a routing number, IBAN, SWIFT code, bank address, and other key details about your account. This document can either be attached to this repository or it can be uploaded to a secure Google Drive folder after opening this issue. We will not be able to process your reimbursement without an official bank proof from your bank. -->


<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"check1::needs triage" ~"check2::needs triage" ~"check3::needs triage"
/confidential
