<!-- This form is for Red Hat associates to receive approval and documentation needed to submit internal Concur expense reports against the Red Hat OSPO cost center. This form should ONLY be used when you have received prior approval in a public ticket with the Fedora Mindshare Committee or CentOS Promo SIG.

NOTE: Make sure your issue is marked as confidential. This way, only members of the Red Hat Open Source Program Office will receive your request. This box should be checked before you open the issue: "This issue is confidential and should only be visible to team members with at least Reporter access."

If you have questions about this form, send them by email to ospo@redhat.com or add a comment to this issue after opening it. -->


## About me
<!-- These questions help us know who you are. -->

* **Full legal name**:

* **FAS username**:

* **Red Hat SSO username**:


## About my event
<!-- These questions help us identify your event. -->

* **Event name**:

* **Event date(s)**:
<!-- Use YYYY-MM-DD date formatting. -->

* **URL to public approval ticket**:
<!-- If you do not have this, you skipped a step. Contact the Fedora or CentOS Community Architects for help completing this step. -->

* **URL to event report and/or social media**:
<!-- All events funded by Red Hat OSPO require you to write an event report or social media posts about the event. If the event has not yet happened, write "TBD". Flock is exempted from this policy and you may write "Flock exemption" here instead (although it is still encouraged for Flock). -->


## About my expenses
<!-- As a Red Hat associate, you are REQUIRED to use Concur for your expenses regardless of whether your Fedora/CentOS event sponsorship is part of your role responsibilities at Red Hat or not. The information below is required for you to receive an email authorization granting you permission to bill against the Red Hat OSPO cost center. -->

* **Total amount budgeted**:
<!-- Total amount approved in the public ticket. You must have this data before opening this issue. -->

* **Concur report ID**:
<!-- The full string of the Concur report ID. Example: 688D2EB14B02423AA088. You can find this in the Report Header menu or in the URL of the Concur report. It is *NOT* the six-character Report Number. If the event has not yet happened yet, write "TBD" instead. You will need to edit this later. -->

* **Concur expense report total**:
<!-- This must match the exact amount of your Concur expense report. Use the default currency for your Concur expense report (this varies by associate country). If the event has not yet happened yet, write "TBD" instead. You will need to edit this later. -->


<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"check1::needs triage" ~"check2::BEAR not required" ~"check3::needs triage"
/confidential
